package it.uniroma2.art.lw.ontolex;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import it.uniroma2.art.lw.model.objects.LIFactory;

public class OntolexBundle implements BundleActivator {

	@Override
	public void start(BundleContext context) throws Exception {
		context.registerService(LIFactory.class.getName(), 
				new OntolexFactory("ONTOLEX"), null);	
	}

	@Override
	public void stop(BundleContext context) throws Exception {
	
	}

}
