package it.uniroma2.art.lw.ontolex;

import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;


public class OntolexConcept extends SemanticIndex {

    private ARTURIResource lexicalConcept;

    public OntolexConcept(ARTURIResource lexicalConcept) {
        this.lexicalConcept = lexicalConcept;
    }

    public OntolexConcept(String uri, OntoLexModel ontoLexModel) throws SemIndexRetrievalException {

        try {

            lexicalConcept = ontoLexModel.createURIResource(uri);

            if (!ontoLexModel.isLexicalConcept(lexicalConcept)) {

                throw new SemIndexRetrievalException();

            }

        } catch (ModelAccessException ex) {
            throw new SemIndexRetrievalException();
        }

    }

    public ARTURIResource getLexicalConcept() {
        return lexicalConcept;
    }

    public void setLexicalConcept(ARTURIResource lexicalConcept) {
        this.lexicalConcept = lexicalConcept;
    }

    @Override
    public String getConceptRepresentation() {
        return lexicalConcept.getURI();
    }

    @Override
    public String toString() {
        return getConceptRepresentation();
    }
}
