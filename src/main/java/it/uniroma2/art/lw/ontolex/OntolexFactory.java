package it.uniroma2.art.lw.ontolex;

import it.uniroma2.art.lw.model.objects.LIFactory;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;

public class OntolexFactory extends LIFactory {


    public OntolexFactory(String id) {
        super(id, OntolexInterface.class);
    }

    @Override
    public LinguisticInterface getLinguisticInterface() {
        OntolexInterface ontolexInterface = new OntolexInterface();        
        return ontolexInterface;
    }

}
