/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.lw.ontolex;

import it.uniroma2.art.lw.exceptions.LinguisticInterfaceLoadException;
import it.uniroma2.art.lw.exceptions.LinguisticResourceAccessException;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LexicalRelation;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchStrategy;
import it.uniroma2.art.lw.model.objects.SearchWord;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemanticRelation;
import it.uniroma2.art.lw.model.objects.TaxonomicalLR;
import it.uniroma2.art.lw.properties.InstanceProperty;
import it.uniroma2.art.ontolex.facilities.OWLArtModelFactoryWithOntoLexModel;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.vocabolary.OntoLex;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.models.impl.SKOSModelImpl;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.vartrans.model.VARTRANSModel;
import it.uniroma2.art.vartrans.model.impl.VARTRANSModelImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class OntolexInterface extends LinguisticInterface implements TaxonomicalLR, LRWithGlosses {
    
    private Map<String, ARTURIResource> lexicalRelationsMap;
    
    private Collection<SemanticRelation> semanticRelations;
    private Collection<LexicalRelation> lexicalRelations;
    private Collection<SearchStrategy> searchStrategies;
    private Collection<SearchFilter> searchFilters;
    
    public OntoLexModel ontoLexModel;
    private SKOSModel skosModel;
    private VARTRANSModel vartransModel;
    
    @InstanceProperty(description = "add a description")
    public String sparqlEndPoint;
    
    @InstanceProperty(description = "add a description")
    public String language;
    
    @InstanceProperty(description = "add a description")
    public String baseURI;
    
    @InstanceProperty(description = "add a description")
    public String persistenceDirectory;

    /*-------------------------------------------*/
    @Override
    public Collection<SemanticIndex> getDirectAncestors(SemanticIndex c) {
        
        ARTURIResource lexicalConcept = ((OntolexConcept) c).getLexicalConcept();
        
        ArrayList<SemanticIndex> result = new ArrayList<SemanticIndex>();
        
        try (ARTURIResourceIterator it = skosModel.listBroaderConcepts(lexicalConcept, false, true)) {
            
            while (it.hasNext()) {
                
                result.add(new OntolexConcept(it.next()));
                
            }
            
        } catch (ModelAccessException ex) {
            
        }
        
        return result;
        
    }
    
    @Override
    public Collection<SemanticIndex> getDirectDescendants(SemanticIndex c) {
        
        ARTURIResource lexicalConcept = ((OntolexConcept) c).getLexicalConcept();
        
        ArrayList<SemanticIndex> result = new ArrayList<SemanticIndex>();
        
        try (ARTURIResourceIterator it = skosModel.listNarrowerConcepts(lexicalConcept, false, true)) {
            
            while (it.hasNext()) {
                
                result.add(new OntolexConcept(it.next()));
                
            }
            
        } catch (ModelAccessException ex) {
            
        }
        
        return result;
        
    }
    
    @Override
    public String[] getConceptLexicals(SemanticIndex c) {
        
        ARTURIResource lexicalConcept = ((OntolexConcept) c).getLexicalConcept();
        HashSet<String> result = new HashSet<String>();
        
        try (ARTURIResourceIterator it = ontoLexModel.listIsEvokedBy(lexicalConcept)) {
            
            while (it.hasNext()) {
                
                ARTURIResource lexicalEntry = it.next();
                
                try (ARTURIResourceIterator it1 = ontoLexModel.listLexicalForms(lexicalEntry)) {
                    
                    while (it1.hasNext()) {
                        
                        ARTURIResource form = it1.next();
                        
                        try (ARTLiteralIterator it2 = ontoLexModel.listWrittenReps(form, language)) {
                            
                            while (it2.hasNext()) {
                                
                                result.add(it2.next().getLabel());
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            }
            
        } catch (ModelAccessException ex) {
            return new String[0];
        }
        
        return result.toArray(new String[result.size()]);
        
    }
    
    @Override
    public Collection<SemanticIndex> getConcepts(SearchWord searchWord) {
        
        HashSet<SemanticIndex> result = new HashSet<SemanticIndex>();
        
        if (searchWord != null) {
            
            try {
                
                String queryString = "select distinct ?lexicalConcept where { "
                        + "?form " + RDFNodeSerializer.toNT(OntoLex.Res.WRITTEN_REP) + " ?rep. "
                        + "?lexicalEntry " + RDFNodeSerializer.toNT(OntoLex.Res.LEXICAL_FORM) + " ?form. "
                        + "?lexicalEntry " + RDFNodeSerializer.toNT(OntoLex.Res.EVOKES) + " ?lexicalConcept. "
                        + "FILTER (?rep=" + RDFNodeSerializer.toNT(ontoLexModel.createLiteral(searchWord.getName().toLowerCase(), language)) + "). "
                        + "}";
                
                TupleQuery query = ontoLexModel.createTupleQuery(QueryLanguage.SPARQL, queryString, null);
                
                try (TupleBindingsIterator it = query.evaluate(true)) {
                    
                    while (it.hasNext()) {
                        
                        TupleBindings next = it.next();
                        
                        result.add(new OntolexConcept(next.getBoundValue("lexicalConcept").asURIResource()));
                    }
                    
                } catch (QueryEvaluationException ex) {
                    
                }
                
            } catch (UnsupportedQueryLanguageException | ModelAccessException | MalformedQueryException ex) {
                
            }
            
        }
        
        return result;
        
    }
    
    @Override
    public SemanticIndex getConcept(String uri) throws SemIndexRetrievalException {
        
        OntolexConcept ontolexConcept = new OntolexConcept(uri, ontoLexModel);
        
        return ontolexConcept;
        
    }

    /*-----------------------------------*/
    @Override
    public void initialize() throws LinguisticInterfaceLoadException {
        
        ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();
        
        OWLArtModelFactoryWithOntoLexModel<? extends ModelConfiguration> fact;
        fact = OWLArtModelFactoryWithOntoLexModel.createModelFactory(factImpl);
        
        try {
            ontoLexModel = fact.loadOntoLexModel(sparqlEndPoint);
        } catch (ModelCreationException ex) {
            throw new LinguisticInterfaceLoadException();
        }
        
        if ((sparqlEndPoint != null && !sparqlEndPoint.isEmpty())
                && ((language != null && !language.isEmpty()))) {
            
            try {
                ontoLexModel = fact.loadOntoLexModel(sparqlEndPoint);
                skosModel = new SKOSModelImpl(ontoLexModel);
                vartransModel = new VARTRANSModelImpl(ontoLexModel);
            } catch (ModelCreationException ex) {
                throw new LinguisticInterfaceLoadException();
            }
            
        } else if ((baseURI != null && !baseURI.isEmpty()) && (language != null && !language.isEmpty())
                && (persistenceDirectory != null && !persistenceDirectory.isEmpty())) {
            
            try {
                
                ontoLexModel = fact.loadOntoLexModel(baseURI, persistenceDirectory);
                skosModel = new SKOSModelImpl(ontoLexModel);
                vartransModel = new VARTRANSModelImpl(ontoLexModel);
            } catch (ModelCreationException ex) {
                throw new LinguisticInterfaceLoadException();
            }
            
        } else {
            throw new LinguisticInterfaceLoadException();
        }
        
        loadSemanticRelations();
        loadLexicalRelations();
        loadSearchStrategies();
        loadSearchFilters();
        
    }
    
    @Override
    public Collection<SemanticIndex> exploreSemanticRelation(SemanticIndex c, SemanticRelation Relation) throws LinguisticResourceAccessException {
        
        Collection<SemanticIndex> result = new ArrayList<SemanticIndex>();
        
        ARTURIResource concept = ((OntolexConcept) c).getLexicalConcept();
        
        switch (Relation.getName()) {
            case "Hypernymy":
                result = getDirectAncestors(c);
                break;
            case "Hyponymy":
                result = getDirectDescendants(c);
                break;
        }
        
        return result;
        
    }
    
    @Override
    public Collection<SemanticIndex> exploreLexicalRelation(String wordString, SemanticIndex c, LexicalRelation Relation) throws LinguisticResourceAccessException {
        
        HashSet<SemanticIndex> result = new HashSet<SemanticIndex>();
        
        ARTURIResource concept = ((OntolexConcept) c).getLexicalConcept();
        
        try {
            WordInOntolexConcept wordInOntolexConcept = new WordInOntolexConcept(concept, wordString, language, ontoLexModel);
            
            HashSet<ARTURIResource> senses = new HashSet<ARTURIResource>();
            
            try (ARTURIResourceIterator it = ontoLexModel.listSenses(wordInOntolexConcept.getWord())) {
                
                while (it.hasNext()) {
                    senses.add(it.next());
                }
                
            }
            
            HashSet<ARTURIResource> senses1 = new HashSet<ARTURIResource>();
            
            try (ARTURIResourceIterator it = ontoLexModel.listLexicalizedSense(concept)) {
                
                while (it.hasNext()) {
                    
                    senses1.add(it.next());
                    
                }
                
            }
            
            senses.retainAll(senses1);
            
            HashSet<ARTURIResource> newSenses = new HashSet<ARTURIResource>();
            
            for (Iterator<ARTURIResource> iterator = senses.iterator(); iterator.hasNext();) {
                ARTURIResource next = iterator.next();
                
                try (ARTURIResourceIterator it = vartransModel.listTargets(next, lexicalRelationsMap.get(Relation.getName()))) {
                    
                    while (it.hasNext()) {
                        
                        newSenses.add(it.next());
                        
                    }
                    
                }
                
            }
            
            for (Iterator<ARTURIResource> iterator = newSenses.iterator(); iterator.hasNext();) {
                ARTURIResource next = iterator.next();
                
                try (ARTURIResourceIterator it = ontoLexModel.listIsLexicalizedSenseOf(next)) {
                    
                    while (it.hasNext()) {
                        
                        result.add(new WordInOntolexConcept(it.next(), ontoLexModel.getIsSenseOf(next)));
                        
                    }
                    
                }
                
            }
            
        } catch (SemIndexRetrievalException | ModelAccessException ex) {
            
            throw new LinguisticResourceAccessException(ex);
        }
        
        return result;
    }
    
    @Override
    protected Collection<SearchWord> getSearchWords(String word, String methodName, SearchFilter... filters) {
        
        Collection<SearchWord> searchedWord = new ArrayList<SearchWord>();
        
        Collection<String> filterList = new ArrayList<String>();
        
        if (filters.length == 0) {
            
            for (SearchFilter searchFilter : searchFilters) {
                
                if (searchFilter.isActive()) {
                    filterList.add(searchFilter.getName());
                }
            }
        } else {
            
            for (SearchFilter searchFilter : filters) {
                
                if (searchFilter == SearchFilter.ANY) {
                    
                    filterList = new ArrayList<String>();
                    
                    for (SearchFilter searchFilterAny : searchFilters) {
                        filterList.add(searchFilterAny.getName());
                    }
                    
                } else if (searchFilter == SearchFilter.NONE) {
                    
                    filterList = new ArrayList<String>();
                    
                } else {
                    filterList.add(searchFilter.getName());
                }
            }
        }
        
        if (filterList.contains("Exact search")) {
            
            try (ARTURIResourceIterator it = ontoLexModel.listFormsByWrittenRep(word, language)) {
                
                if (it.hasNext()) {
                    
                    searchedWord.add(new SearchWord(word));
                }
            } catch (ModelAccessException ex) {
                
            }
            
        }
        
        return searchedWord;
    }
    
    @Override
    public Collection<SemanticRelation> getSemanticRelations() {
        
        return semanticRelations;
    }
    
    @Override
    public Collection<LexicalRelation> getLexicalRelations() {
        
        return lexicalRelations;
    }
    
    @Override
    public Collection<SearchStrategy> getSearchStrategies() {
        
        return searchStrategies;
    }
    
    @Override
    public Collection<SearchFilter> getSearchFilters() {
        
        return searchFilters;
    }
    
    @Override
    public SemanticRelation getSemanticRelation(String semRelation
    ) {
        
        for (SemanticRelation relation : semanticRelations) {
            if (relation.getName().equals(semRelation)) {
                return relation;
            }
        }
        return null;
        
    }
    
    @Override
    public LexicalRelation getLexicalRelation(String lexRelation
    ) {
        
        for (LexicalRelation relation : lexicalRelations) {
            if (relation.getName().equals(lexRelation)) {
                return relation;
            }
        }
        return null;
        
    }
    
    @Override
    public SearchStrategy getSearchStrategy(String searchStrategy
    ) {
        
        for (SearchStrategy strategy : searchStrategies) {
            if (strategy.getName().equals(searchStrategy)) {
                return strategy;
            }
        }
        return null;
        
    }
    
    @Override
    public SearchFilter getSearchFilter(String searchFilter
    ) {
        
        for (SearchFilter filter : searchFilters) {
            if (filter.getName().equals(searchFilter)) {
                return filter;
            }
        }
        return null;
        
    }
    /*-----------------------------------*/
    
    @Override
    public String getConceptGloss(SemanticIndex c) {
        
        String gloss = "";
        
        ARTURIResource lexicalConcept = ((OntolexConcept) c).getLexicalConcept();
        
        try (ARTLiteralIterator it = skosModel.listDefinitions(lexicalConcept, language, true)) {
            
            if (it.hasNext()) {
                
                gloss = it.next().getLabel();
                
            }
            
        } catch (ModelAccessException ex) {
            
        }
        
        String writtenRep = getWord(c);
        
        if (!(gloss.isEmpty() || writtenRep.isEmpty())) {
            gloss += " : ";
        }
        
        gloss += writtenRep;
        
        return gloss;
        
    }
    
    @Override
    public String getWord(SemanticIndex c) {
        
        if (c instanceof WordInOntolexConcept) {
            ARTURIResource lexicalEntry = ((WordInOntolexConcept) c).getWord();
            
            try {
                ARTURIResource canonicalForm = ontoLexModel.getCanonicalForm(lexicalEntry);
                
                if (canonicalForm != null) {
                    
                    try (ARTLiteralIterator it = ontoLexModel.listWrittenReps(canonicalForm, language)) {
                        
                        if (it.hasNext()) {
                            
                            return it.next().getLabel();
                            
                        }
                        
                    }
                    
                } else {
                    
                    try (ARTURIResourceIterator it = ontoLexModel.listOtherForms(lexicalEntry)) {
                        
                        if (it.hasNext()) {
                            
                            try (ARTLiteralIterator it1 = ontoLexModel.listWrittenReps(it.next(), language)) {
                                
                                if (it1.hasNext()) {
                                    
                                    return it1.next().getLabel();
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                }
                
            } catch (ModelAccessException ex) {
                
            }
            
        }
        
        return "";
    }

    /*-----------------------------------*/
    private void loadSemanticRelations() {
        
        semanticRelations = new ArrayList<SemanticRelation>();
        semanticRelations.add(new SemanticRelation("Hypernymy"));
        semanticRelations.add(new SemanticRelation("Hyponymy"));
        
    }
    
    private void loadLexicalRelations() {
        
        lexicalRelations = new ArrayList<LexicalRelation>();
        lexicalRelationsMap = new HashMap<>();
        
        try (ARTURIResourceIterator it = vartransModel.listSenseRel()) {
            
            while (it.hasNext()) {
                
                ARTURIResource next = it.next();
                
                lexicalRelationsMap.put(next.getLocalName(), next);
                lexicalRelations.add(new LexicalRelation(next.getLocalName()));
                
            }
            
        } catch (ModelAccessException ex) {
            
        }
        
    }
    
    private void loadSearchStrategies() {
        
        searchStrategies = new ArrayList<SearchStrategy>();
        
        searchStrategies.add(new SearchStrategy("all lemma"));
    }
    
    private void loadSearchFilters() {
        
        searchFilters = new ArrayList<SearchFilter>();
        
        SearchFilter searchFilter;
        searchFilter = new SearchFilter("Exact search");
        searchFilter.setActive(true);
        searchFilters.add(searchFilter);
    }
    
}
