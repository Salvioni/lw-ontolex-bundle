package it.uniroma2.art.lw.ontolex;

import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.vocabolary.OntoLex;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

public class WordInOntolexConcept extends OntolexConcept {

    ARTURIResource word;

    public WordInOntolexConcept(ARTURIResource lexicalConcept, String writtenRep, String language, OntoLexModel ontoLexModel) throws SemIndexRetrievalException {

        super(lexicalConcept);

        boolean found = false;

        try (ARTURIResourceIterator it = ontoLexModel.listFormsByWrittenRep(writtenRep, language)) {

            while (it.hasNext() && !found) {

                ARTURIResource form = it.next();

                try (ARTURIResourceIterator it1 = ontoLexModel.listLexicalEntriesByLexicalForm(form)) {

                    while (it1.hasNext()) {

                        ARTURIResource next = it1.next();
                        if (ontoLexModel.hasTriple(lexicalConcept,OntoLex.Res.IS_EVOKED_BY, next, true)) {
                            word = next;
                            found = true;
                        }
                    }

                }

            }

        } catch (ModelAccessException ex) {

        }

        if (word == null) {
            throw new SemIndexRetrievalException();
        }

    }

    public WordInOntolexConcept(ARTURIResource lexicalConcept, ARTURIResource word) {

        super(lexicalConcept);
        this.word = word;
    }

    public ARTURIResource getWord() {
        return word;
    }

}
